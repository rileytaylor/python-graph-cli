# Python Graph Test

A simple cli application to work with the Microsoft Graph API. Currently it only implements things related to calendaring.

## Getting Started

**Prerequisites:** You need to have poetry installed and be utilizing pyenv before proceeding.

You'll need to configure environment variables.

TODO

```sh
poetry install
# Login with your Microsoft account
poetry run python cli.py login
# See what commands are avaialable
poetry run python cli.py --help
```

## Project Structure

- **config/**: holds the `.env` files and reads them in `settings.py`
- **events/**: sample event .json files for use by the cli
- **lib/**: functions that make things run
  - **auth.py**: contains authentication-related functions
  - **calendars.py**: contains calendar endpoint functions
  - **events.py**: contains events endpoint functions
  - **users.py**: contains user endpoint functions
- **cli.py**: contains the actual cli itself, and is the root of the application
- **cli-app.py**: contains a version of the cli that uses application authentication, allowing editing of other calendars

## Under the Hood

This tool makes use of [click](https://click.palletsprojects.com/en/) to structure the cli interactions. All api requests are handled through [requests](https://2.python-requests.org/en/master/). This cli stores authentication tokens (very unsecurely, but good enough for a small demo) with python's [shelve](https://docs.python.org/3/library/shelve.html) library.

Authentication with the Graph API is handled via [adal](https://github.com/AzureAD/azure-activedirectory-library-for-python), an Azure AD auth library proviced by the Azure team for this purpose. All of the auth could be done via requests, but this simplifies things a bit. Their documentation isn't fully complete in making it obvious how to handle refresh tokens, so this provides a nice example of a flow that automatically refreshes the primary token without bothering the user.

## Sample Commands

To see all commands, use the help option:

```sh
poetry run python cli.py --help
poetry run python cli-app.py --help
```

Further information can be found for each command as well (`create --help`, etc.)

To create an event on a calendar called `Yeet`:

```sh
poetry run python cli.py --calendar 'Yeet' events/1.json
```

For now, events need to exist as json files. The `events/` folder has examples. If the `Yeet` calendar doesn't exist, it'll be created.
