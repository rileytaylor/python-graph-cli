"""Load enviornment variables and control cli settings"""

from environs import Env

env = Env()
env.read_env()

APP_DOMAIN_ID = env.str("APP_DOMAIN_ID")
CLIENT_ID = env.str("CLIENT_ID")

TESTOU_APP_DOMAIN_ID = env.str("TESTOU_APP_DOMAIN_ID")
TESTOU_CLIENT_ID = env.str("TESTOU_CLIENT_ID")
TESTOU_CLIENT_SECRET = env.str("TESTOU_CLIENT_SECRET")
