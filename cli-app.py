import json

import click
import requests

from lib.users import running_as_user
from lib.calendars import verify_or_create_calendar
from lib.auth import auth, get_session, get_session_as_app
from lib.events import create_event, update_event, delete_event


@click.group()
@click.pass_context
def cli(ctx):
    session = get_session_as_app()

    ctx.obj = {
        'session': session
    }

@click.command('checkauth')
@click.pass_obj
def checkauth(context):
    """Verify that the cli is authenticated with Azure AD"""
    if context['session']:
        print('App Authentication successful!')

@click.command('whoami')
@click.option('-u',
              '--user',
              required=False,
              type=str,
              help='A user id or principalName'
)
@click.pass_obj
def who_am_i(context, user):
    """Just return the user of the id"""
    if context['session']:
        if user:
            running_as_user(context['session'], user)
        else:
            print('To run as an application the `--user` attribute is required')

@cli.command('create')
@click.option('-c',
              '--calendar',
              required=True,
              type=str,
              help='The calendar to send events to'
)
@click.option('-u',
              '--user',
              required=False,
              type=str,
              help='A user id or principalName'
)
@click.argument('event', type=click.File('rb'))
@click.pass_obj
def create(context, calendar, event, user):
    """Create an event"""
    if context['session']:
        if user:
            running_as_user(context['session'], user)
            event_data = json.loads(event.read())
            calendar_id = verify_or_create_calendar(context['session'], calendar, user)
            created_event = create_event(context['session'], calendar_id, event_data, user)
            if created_event:
                print(f'Event created: {created_event["webLink"]}')
            else:
                print('Unable to create event')
        else:
            print('To run as an application the `--user` attribute is required')

@click.command('update')
@click.pass_obj
def update(context, user):
    """Update an event"""
    if context['session']:
        if user:
            running_as_user(context['session'])
        else:
            print('To run as an application the `--user` attribute is required')

@click.command('delete')
@click.pass_obj
def delete(context):
    """Delete an event"""
    if context['session']:
        running_as_user(context['session'])

cli.add_command(checkauth)
cli.add_command(who_am_i)
cli.add_command(create)
cli.add_command(update)
cli.add_command(delete)

if __name__ == "__main__":
    cli()