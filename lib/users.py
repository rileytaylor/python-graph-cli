"""Users graph api methods"""

import requests

def running_as_user(session, user=None):
    """Prints the username that the command will run for"""
    # If we have a user id or principalName, use it. Otherwise call `me`
    if user:
        path = f'users/{user}'
    else:
        path = 'me'

    try:
        r = session.get(f'https://graph.microsoft.com/v1.0/{path}')
        r.raise_for_status()
        data = r.json()
        print(f'Logged in as: {data["givenName"]} {data["surname"]} (Principal Name: {data["userPrincipalName"]}, id: {data["id"]})')
    except requests.exceptions.HTTPError as eh:
        print("Http Error: ", eh)
    except requests.exceptions.ConnectionError as ec:
        print("Error Connecting: ", ec)
    except requests.exceptions.Timeout as et:
        print("Timeout Error: ", et)
    except requests.exceptions.RequestException as er:
        print("Error: ", er)