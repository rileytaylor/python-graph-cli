""" Functions dealing with the calendar endpoint"""

import requests

def calendar_exists(session, name, path):
    """
    Verify the existence of the Trellis Advise calendar
    returns: (string) a calendar id or a blank string
    """

    try:
        r = session.get(f'https://graph.microsoft.com/v1.0/{path}/calendars')
        r.raise_for_status() # TODO: Verify this fails properly
        data = r.json()
        calendars = [c['name'] for c in data['value']]
        if name in calendars:
            # Return the calendar id if the calendar exists
            return [c['id'] for c in data['value'] if c['name'] == name][0]
        else:
            # Return a blank string if the calendar doesn't exist (which is falsy)
            return ''
    except requests.exceptions.HTTPError as eh:
        print("Http Error: ", eh)
    except requests.exceptions.ConnectionError as ec:
        print("Error Connecting: ", ec)
    except requests.exceptions.Timeout as et:
        print("Timeout Error: ", et)
    except requests.exceptions.RequestException as er:
        print("Error: ", er)

def verify_or_create_calendar(session, name, user=None):
    """
    Create the calendar if it doesn't exist
    returns: a calendar id
    """
    if user:
        path = f'users/{user}'
    else:
        path = 'me'

    existing_cal = calendar_exists(session, name, path)
    # Create the calendar if it doesn't exist
    if not existing_cal:
        print('Calendar does not exist, creating calendar')
        try:
            r = session.post(
                f'https://graph.microsoft.com/v1.0/{path}/calendars',
                json={'name': name}
            )
            r.raise_for_status()
            data = r.json()
            print(f'Calendar {data["name"]} created.')
            # Return the new calendar id
            return data['id']
        except requests.exceptions.HTTPError as eh:
            print("Http Error: ", eh)
        except requests.exceptions.ConnectionError as ec:
            print("Error Connecting: ", ec)
        except requests.exceptions.Timeout as et:
            print("Timeout Error: ", et)
        except requests.exceptions.RequestException as er:
            print("Error: ", er)
    # If it exists, return the id
    return existing_cal

def get_calendar(session, calendar_id, user=None):
    """Get the details of a calendar"""
    if user:
        path = f'users/{user}'
    else:
        path = 'me'

    try:
        r = session.get(
            f'https://graph.microsoft.com/v1.0/{path}/calendar'
        )
    except requests.exceptions.HTTPError as eh:
        print("Http Error: ", eh)
    except requests.exceptions.ConnectionError as ec:
        print("Error Connecting: ", ec)
    except requests.exceptions.Timeout as et:
        print("Timeout Error: ", et)
    except requests.exceptions.RequestException as er:
        print("Error: ", er)